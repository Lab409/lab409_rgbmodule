#!/bin/python

# This program designed to testing RGB LED on RPi using RPIO library
# Author: Danil Borchevkin
# Mail: danil.borchevkin@gmail.com

import RPIO
from time import sleep

RED = 17
GREEN = 27
BLUE = 22

def RGB_Init():
    '''
    This function init GPIO pins for RGB LED and set off all colors
    '''
    RPIO.setwarnings(False)     # Off warnings of changing GPIO state
    
    RPIO.setup(RED, RPIO.OUT, initial = RPIO.HIGH)
    RPIO.setup(GREEN, RPIO.OUT, initial = RPIO.HIGH)
    RPIO.setup(BLUE, RPIO.OUT, initial = RPIO.HIGH)
    
    return None


def RGB_SetColor(color, state):
    '''
    Set color in state
    state may be "1" to on color or "0" to off the color
    
    (int,int) -> (None)
    '''
    
    if state == 1:    
       RPIO.output(color, RPIO.LOW)
    elif state == 0:
       RPIO.output(color, RPIO.HIGH)
    
    return None


def RGB_SetRGBColor(r, g, b):
    '''
    Set color in RGB format

    (int, int, int) -> (None)
    '''

    RGB_SetColor(RED, r)
    RGB_SetColor(GREEN, g)
    RGB_SetColor(BLUE, b)

    return None


def RGB_RedOn():
    '''
    Set on Red color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(1, 0, 0)
    return None


def RGB_GreenOn():
    '''
    Set on Green color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(0, 1, 0)
    return None


def RGB_BlueOn():
    '''
    Set on Blue color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(0, 0, 1)
    return None


def RGB_YellowOn():
    '''
    Set on Yellow color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(1, 1, 0)
    return None


def RGB_MagentaOn():
    '''
    Set on Magenta color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(1, 0, 1)
    return None


def RGB_CyanOn():
    '''
    Set on Cyan color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(0, 1, 1)
    return None


def RGB_WhiteOn():
    '''
    Set on White color
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(1, 1, 1)
    return None


def RGB_Off():
    '''
    Set off all colors (red, green, and blue)
    
    (None) -> (None)
    '''
    RGB_SetRGBColor(0, 0, 0)
    return None


def RGB_Flashing(func, times, delay):
    '''
    Flashing desired color.
    Func - is callback function;
    Delay in seconds. Delay may be < 1
    
    (function, float, delay) -> None
    '''   

    for i in range(times):
        func()
        sleep(delay)
        RGB_Off()
        sleep(delay)        

    return None


if __name__=='__main__':
    # Testing sequence
    
    RGB_Init()
    
    RGB_Flashing(RGB_RedOn, 3, 0.1)
    RGB_Flashing(RGB_GreenOn, 3, 0.1)
    RGB_Flashing(RGB_BlueOn, 3, 0.1)
    RGB_Flashing(RGB_MagentaOn, 3, 0.1)
    RGB_Flashing(RGB_CyanOn, 3, 0.1)
    RGB_Flashing(RGB_YellowOn, 3, 0.1)
    RGB_Flashing(RGB_WhiteOn, 3, 0.1)
    
    RGB_RedOn()
    sleep(0.5)
    RGB_GreenOn()
    sleep(0.5)
    RGB_BlueOn()
    sleep(0.5)
    RGB_MagentaOn()
    sleep(0.5)
    RGB_CyanOn()
    sleep(0.5)
    RGB_YellowOn()
    sleep(0.5)
    RGB_WhiteOn()
    sleep(0.5)
    
    RGB_Off()
    
    #RPIO.cleanup()             #clean all setting for GPIO before exit of program
